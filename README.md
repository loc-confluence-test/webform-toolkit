# Webform Toolkit [<img src="https://travis-ci.org/nuxy/Webform-Toolkit.svg?branch=master" alt="Build Status" />](https://travis-ci.org/nuxy/Webform-Toolkit)

Dynamically generate an HTML form with field validation and custom errors from JSON

[<img src="https://nuxy.github.io/Webform-Toolkit/preview.jpg" alt="Webform-Toolkit" />](https://nuxy.github.io/Webform-Toolkit)

## Features

- Extensible HTML/CSS interface.
- Compatible with Firefox 3.6, Chrome, Safari 5, Opera, and Internet Explorer 7+ web browsers.
- Compatible with iOS and Android mobile web browsers.
- Easy to set-up and customize.
- Provides form input validation using REGEX ([regular expressions](http://www.regular-expressions.info/reference.html))
- Supports synchronous form-data POST
- Supports FORM submit callback for custom AJAX handling.
- Supports dynamic ([on the fly](https://nuxy.github.io/Webform-Toolkit/#methods)) field creation.
- Fast and lightweight (jQuery plug-in *only 7.2 kB)

## Installation

This package can be easily installed using [Bower](http://bower.io).

    $ bower install webform-toolkit

Please refer to the [README](https://nuxy.github.io/Webform-Toolkit) for more information about this package.

## License and Warranty

This package is distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchantability or fitness for a particular purpose.

_Webform-Toolkit_ is provided under the terms of the [MIT license](http://www.opensource.org/licenses/mit-license.php)

## Author

[Marc S. Brooks](https://github.com/nuxy)
